// @flow

import BaseModel from './BaseModel'
import S3Storage from './S3Storage'

export default class PictureStorage extends BaseModel {

    s3: S3Storage

    constructor(s3: S3Storage) {
        super()

        this.s3 = s3
    }

    get(identifier: string, bucket: string): Promise<Object> {
        return this.s3.getObject(identifier, bucket).then((data) => {
            return {
                image: data.Body,
                contentType: data.contentType,
            }
        })
    }
}
