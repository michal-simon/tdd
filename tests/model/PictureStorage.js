/* eslint-disable no-console */
'use strict'

import {describe, it,} from 'mocha';
import {assert} from 'chai';
import PictureStorage from '../../app/model/PictureStorage'

describe('Picture Storage', () => {
    it('should retrieve a picture', () => {
        const identifier = 'my-test-identifier.jpg'

        const s3Storage = {
            getObject: (name) => {
                assert.equal(identifier, name, 'File name is not correct')

                return Promise.resolve({
                    Body: 'test-body',
                    contentType: 'image/jpg',
                })
            },
        };

        const pictureStorage = new PictureStorage(s3Storage)

        pictureStorage.get(identifier)
    })
})
