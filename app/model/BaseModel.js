// @flow

export default class BaseModel {
    constructor() {
        if (this.constructor === BaseModel) {
            // Abstract class can not be instantiated directly.
            throw new TypeError('Can not instantiate abstract class BaseModel.')
        }
    }
}
