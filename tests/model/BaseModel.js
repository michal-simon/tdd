/* eslint-disable no-console */
'use strict'

import {describe, it,} from 'mocha';
import chai from 'chai'
import BaseModel from "../../app/model/BaseModel";

describe('Base Model', () => {
    it('should throw an error', () => {
        chai.expect(() => new BaseModel()).to.throw('Can not instantiate abstract class BaseModel.')
    })
})
