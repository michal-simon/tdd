// @flow

import AWS from 'aws-sdk'
import BaseModel from './BaseModel'

export default class S3Storage extends BaseModel {

    getObject(key: string, bucket: string): Promise<void> {
        const s3 = new AWS.S3({
            apiVersion: '2006-03-01',
        })

        return s3.getObject({
            Bucket: bucket,
            Key: key,
        }).promise()
    }
}
