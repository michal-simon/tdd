// @flow

import BaseModel from './BaseModel'
import S3Storage from './S3Storage'

export default class ArticleModel extends BaseModel {    
    async list(articles) {
      let results = []
      let article_path = "/blog/articles/"
      
      for (const article of articles) {
        results.push(await this.fetch(article))
      }
      
      return [results, article_path] 
    }
    
    async fetch (article) {
        return Promise.resolve(article)
    }
    
    s3: S3Storage
    
    
    constructor(s3: S3Storage) {
        super()

        this.s3 = s3
    }
}
